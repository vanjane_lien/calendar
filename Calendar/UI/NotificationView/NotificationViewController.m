//
//  NotificationViewController.m
//  Calendar
//
//  Created by Thuy Lien Nguyen on 11/10/14.
//  Copyright (c) 2014 Thuy Lien Nguyen. All rights reserved.
//

#import "NotificationViewController.h"
#import "SWRevealViewController.h"

@interface NotificationViewController ()

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setTitle:@"Notification"];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    btnBack.target = self.revealViewController;
    btnBack.action = @selector(revealToggle:);
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

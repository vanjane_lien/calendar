//
//  MenuTableViewCell.h
//  Calendar
//
//  Created by Thuy Lien Nguyen on 11/10/14.
//  Copyright (c) 2014 Thuy Lien Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell


@property (nonatomic) IBOutlet UILabel *title;

@end

//
//  SideMenuViewController.m
//  Calendar
//
//  Created by Thuy Lien Nguyen on 11/10/14.
//  Copyright (c) 2014 Thuy Lien Nguyen. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SWRevealViewController.h"
#import "MenuTableViewCell.h"

@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

- (void) viewWillAppear:(BOOL)animated {
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    self.navigationItem.title = @"menu";
    
    menuData = @{@"Section1" : @[@"row1", @"row2", @"row3", @"row4"],
                 @"Section2" : @[@"row1"]};
    groupSectionTitles = [menuData allKeys];
    
    [btnMenu addTarget:self.revealViewController action:@selector(revealToggleAnimated:) forControlEvents:UIControlEventTouchUpInside];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table DataSources an Delegate
#pragma mark - header

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [groupSectionTitles count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 70.f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [groupSectionTitles objectAtIndex:section];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 70.f)];
    view.backgroundColor = [UIColor clearColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(26, 30, tableView.frame.size.width, 40)];
    [label setFont:[UIFont boldSystemFontOfSize:16]];
    label.textColor = [UIColor whiteColor];
    NSString *string =[groupSectionTitles objectAtIndex:section];
    
    [label setText:string];
    [view addSubview:label];
    
    return view;
}

#pragma mark - row
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSString *sectionTitle = [groupSectionTitles objectAtIndex:section];
    NSArray *sectionMenuData = [menuData objectForKey:sectionTitle];
    return [sectionMenuData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"MenuTableViewCell";
    
    MenuTableViewCell *cell = (MenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    [cell.backgroundView setBackgroundColor:[UIColor clearColor]];
    
    NSString *sectionTitle = [groupSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionMenuData = [menuData objectForKey:sectionTitle];
    NSString *title = [sectionMenuData objectAtIndex:indexPath.row];
    cell.title.text = title;
    cell.title.textColor = [UIColor whiteColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



@end

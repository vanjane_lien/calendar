//
//  SideMenuViewController.h
//  Calendar
//
//  Created by Thuy Lien Nguyen on 11/10/14.
//  Copyright (c) 2014 Thuy Lien Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UITableView *tblMasterList;
    
    NSDictionary *menuData;
    NSArray *groupSectionTitles;
}

@end

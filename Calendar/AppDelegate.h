//
//  AppDelegate.h
//  Calendar
//
//  Created by Thuy Lien Nguyen on 11/10/14.
//  Copyright (c) 2014 Thuy Lien Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SWRevealViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SWRevealViewController *viewController;


@end

